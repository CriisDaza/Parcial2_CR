<?php 
if(isset($_GET["edition"])){
    $ed=$_GET["edition"];
}else{
    $ed=0;
}
?>
<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <span class="navbar-text">
      Parcial 2 Cristian David Restrepo Daza 20191578030
    </span>
  </div>
</nav>
<div class="row mt-3">
	<div class="col-lg-4"></div>
	<div class="col-lg-4 text-center">
		<div class="form-group">
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<label class="input-group-text">Edition</label>
				</div>
				<select class="form-select" id="edition">
					<option value="0" <?php if($ed==0) echo "selected"?>>Select edition</option>
					<option value="3"  <?php if($ed==3) echo "selected"?>>2020</option>
					<option value="2" <?php if($ed==2) echo "selected"?> >2019</option>
					<option value="1" <?php if($ed==1) echo "selected"?>>2018</option>
				</select>

			</div>
		</div>
	</div>	
</div>
<div id="result"></div>
<script>

	$("#edition").change(function(){		
	//	if($("#edition").val()!=-1){
			
			//var path = "estadisticas.php"+edition;			
			//$("#result").load(path);
		//}else{
		//$("#result").html("");
		//}
		if($("#edition").val()!=0){
		   $("#result").html("<div class='text-center'><img src='img/gifC.gif!w340'></div>");
		 var edition = $("#edition").val();	 
		var url = "index.php?pid=<?php echo base64_encode("presentacion/estadisticas.php") ?>&edition=" + edition;
		location.replace(url);
		}else{
			var url = "index.php?pid=<?php echo base64_encode("presentacion/inicio.php") ?>&edition=" + edition;
			location.replace(url);
		}
	});
</script>