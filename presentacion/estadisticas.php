<?php


if(isset($_GET["edition"])){
    
    $edition=$_GET["edition"];
$edition = new EditionTopic($_GET["edition"]);
$estado = $edition -> AcceptedPapers();
$tema = $edition ->PapersByTopic();

}
include 'presentacion/inicio.php';
?>

<div class="container">
	            <h1> Accepted Papers</h1>
					<div id="estado" style= "height: 400px;" class="text-center"></div>
					<h1> Papers By Topic </h1>
					<br></br>
					<div id="tema" style="height: 400px;" class="text-center"></div>
					
</div>

<script type="text/javascript">
google.charts.load("current", {packages:['corechart','bar']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    
    var dataEstado = google.visualization.arrayToDataTable([
        ["Estado", "Cantidad"],
        <?php 
         foreach ($estado as $p)
            echo "['" . $p[0] . "', " . $p[1] . "],\n";        
        
        ?>
    ]);
    
    var viewEstado = new google.visualization.DataView(dataEstado);
    
    var optionsEstado = {
        bar: {groupWidth: "95%"},
        legend: { position: "right" },
    };
    var chartEstado = new google.visualization.PieChart(document.getElementById("estado"));
    chartEstado.draw(viewEstado, optionsEstado);


    
    var dataTema = google.visualization.arrayToDataTable([
        ['Tema', 'Accepted', 'Rejected'],
        <?php 
                foreach ($tema as $p)
                    echo "['" . $p[0] . "', " . $p[1] . ", " . $p[2] ."],\n";        
               
               ?>
      ]);

      var optionsTema = {
        chart: {
        	
             legend: { position: "right" },
          
        }
      };

      var chartTema = new google.charts.Bar(document.getElementById("tema"));

      chartTema.draw(dataTema,(optionsTema));
    }

</script>

