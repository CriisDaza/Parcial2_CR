<?php
class Conec{
    private $mysql;
    private $resultado;
    
    public function Abrir(){
        $this->mysql = new mysqli("localhost", "root", "", "estadisticas");
        $this->mysql->set_charset("utf8");
    }
    
    public function Cerrar() {
        $this->mysql->close();
    }
    
    public function Ejecutar($sentencia){
        $this -> resultado = $this -> mysql -> query($sentencia);
    }
    
    public function Extraer(){
        return $this -> resultado -> fetch_row();
    }
    
    public function Numfilas(){
        return ($this -> resultado != null) ? $this -> resultado -> num_rows:0;
    }
}