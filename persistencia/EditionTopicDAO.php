<?php

class EditionTopicDAO
{
    
    private $edition_idEdition;
    
    public function EditionTopicDAO($id=""){
        
        $this -> edition_idEdition = $id;
        
    }
    
    public function AcceptedPapers(){
        
        return "select 'Accepted', sum(accepted)
                from editiontopic 
                where edition_idEdition ='". $this -> edition_idEdition . "'
               union select 'Rejected', sum(rejected) from editiontopic 
               where edition_idEdition='". $this ->edition_idEdition."'";
    }
    
    public function PapersByTopic () {
        return "select topic.name, sum(accepted), sum(rejected)
                from editiontopic inner join topic on topic.idTopic = editiontopic.topic_idTopic 
                inner join edition on edition.idEdition = editiontopic.edition_idEdition 
                WHERE edition.idEdition ='" . $this ->edition_idEdition . "'
                GROUP BY  1 ";
    }
}

