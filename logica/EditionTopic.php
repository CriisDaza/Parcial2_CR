<?php 
require_once 'persistencia/EditionTopicDAO.php';
require_once 'persistencia/Conec.php';

class EditionTopic{
    
     private $ided;
     private $conexion;
     private $editionTopicDAO;
    
     public function getIded()
    {
        return $this->ided;
    }

    public function EditionTopic($id=""){
         
         $this -> ided =$id;
         $this -> conexion = new Conec();
         $this -> editionTopicDAO = new EditionTopicDAO($id);
     }
     
     
     public function AcceptedPapers(){
         
         $this -> conexion ->Abrir();
         $this -> conexion ->Ejecutar($this -> editionTopicDAO -> AcceptedPapers());
         $papers= array();
         while(($resultado = $this -> conexion -> extraer()) != null){
             array_push($papers, $resultado);
         }
         
         $this -> conexion ->Cerrar();
         return $papers;
         
     }
     
     public function PapersByTopic () {
         $this -> conexion ->Abrir();
         $this -> conexion ->Ejecutar($this -> editionTopicDAO -> PapersByTopic());
         $papersbytopic= array();
         while(($resultado = $this -> conexion -> extraer()) != null){
             array_push($papersbytopic, $resultado);
         }
         
         $this -> conexion ->Cerrar();
         return $papersbytopic;
         
     }
     
}
